/*
 * CSE 351 Lab 1b (Manipulating Bits in C)
 *
 * Name(s):Tyler Richardson  
 * NetID(s): tjr20
 *
 * This is a file for managing a store of various aisles, represented by an
 * array of 64-bit integers. See aisle_manager.c for details on the aisle
 * layout and descriptions of the aisle functions that you may call here.
 *
 * Written by Porter Jones (pbjones@cs.washington.edu)
 */

#include <stddef.h>  // To be able to use NULL
#include "aisle_manager.h"
#include "store_client.h"
#include "store_util.h"
#include <stdio.h>

// Number of aisles in the store
#define NUM_AISLES 10

#define ZERO 0x0

#define TWO 2

// Number of sections per aisle
#define SECTIONS_PER_AISLE 4

// Number of items in the stockroom (2^6 different id combinations)
#define NUM_ITEMS 64

// Global array of aisles in this store. Each unsigned long in the array
// represents one aisle.
unsigned long aisles[NUM_AISLES];

// Array used to stock items that can be used for later. The index of the array
// corresponds to the item id and the value at an index indicates how many of
// that particular item are in the stockroom.
int stockroom[NUM_ITEMS];


/* Starting from the first aisle, refill as many sections as possible using
 * items from the stockroom. A section can only be filled with items that match
 * the section's item id. Prioritizes and fills sections with lower addresses
 * first. Sections with lower addresses should be fully filled (if possible)
 * before moving onto the next section.
 */
void refill_from_stockroom() {
 unsigned id;
 int items_needed;
 int stock_left;
 for(int i =ZERO; i < NUM_AISLES;i++){
  if(aisles[i] > ZERO){
   for (int j=ZERO; j<SECTIONS_PER_AISLE; j++){
    items_needed = (NUM_AISLES - num_items(&aisles[i],j));
    id = get_id(&aisles[i],j); 
    stock_left = stockroom[id];
    if(items_needed > ZERO && stock_left > ZERO){
     if(items_needed<stock_left){
      add_items(&aisles[i], j ,stock_left);
      stockroom[id]-=items_needed;
     }else if(items_needed >= stock_left){
      add_items(&aisles[i], j ,stock_left);
      stockroom[id]=ZERO;  
     }
    }
   } 
  }
 }
}

/* Remove at most num items from sections with the given item id, starting with
 * sections with lower addresses, and return the total number of items removed.
 * Multiple sections can store items of the same item id. If there are not
 * enough items with the given item id in the aisles, first remove all the
 * items from the aisles possible and then use items in the stockroom of the
 * given item id to finish fulfilling an order. If the stockroom runs out of
 * items, you should remove as many items as possible.
 */
int fulfill_order(unsigned short id, int num) {
unsigned short id_aisle; 
int items_needed=num;
int items_in_aisle;
int items;
for(int i =ZERO; i < NUM_AISLES;i++){
 for (int j=ZERO; j<SECTIONS_PER_AISLE; j++){
  items_in_aisle = num_items(&aisles[i],j);
  id_aisle = get_id(&aisles[i],j);
  if (id == id_aisle){
   if(items_in_aisle > ZERO && items_needed > ZERO){ 
   if(items_needed < items_in_aisle){
    remove_items(&aisles[i], j ,items_needed);
    items_needed=ZERO; 
    goto outofloop;
   }else if(items_in_aisle <= items_needed){
    remove_items(&aisles[i], j ,items_in_aisle);
    items_needed-=items_in_aisle;
   }
   }	         
  }
 }
}
outofloop:
if(stockroom[id] > ZERO ){
 if(items_needed > ZERO && stockroom[id] > items_needed){
  stockroom[id]-= items_needed; 
  items_needed = ZERO;
 }else if(items_needed > stockroom[id]){
  items_needed-=stockroom[id];
  stockroom[id] = ZERO;
 }
}
items = num - items_needed;
return items;
}

/* Return a pointer to the first section in the aisles with the given item id
 * that has no items in it or NULL if no such section exists. Only consider
 * items stored in sections in the aisles (i.e., ignore anything in the
 * stockroom). Break ties by returning the section with the lowest address.
 */
unsigned short* empty_section_with_id(unsigned short id) {
   int id_aisle;
   unsigned short items; 
   char* ptr = NULL;
   for(int i =ZERO; i < NUM_AISLES;i++){
    for (int j=ZERO; j< SECTIONS_PER_AISLE; j++){
     id_aisle = get_id(&aisles[i],j);
    ptr = (char*) (&aisles[i]);
     if(id == id_aisle){
      items = num_items(&aisles[i], j);
      if(items == ZERO){ 
      ptr =(char*) (&aisles[i])+(j*TWO); 
      return (unsigned short*) ptr;
      }
     }
    } 
   }
  return NULL;
}

/* Return a pointer to the section with the most items in the store. Only
 * consider items stored in sections in the aisles (i.e., ignore anything in
 * the stockroom). Break ties by returning the section with the lowest address.
 */
unsigned short* section_with_most_items() {
unsigned short items, last_items = ZERO;
char* ptr = (char*) &aisles[ZERO];
 for(int i =ZERO; i < NUM_AISLES;i++){
  for (int j=ZERO; j< SECTIONS_PER_AISLE; j++){
   
    items = num_items(&aisles[i], j);
   if(items > last_items){
     last_items = items;   
     ptr =(char*) (&aisles[i])+(j*TWO);
    }
  }
 }
  return (unsigned short*) ptr;
}
