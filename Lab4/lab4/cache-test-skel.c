/*
 * CSE 351 Lab 4 (Caches and Cache-Friendly Code)
 * Part 1 - Inferring Mystery Cache Geometries
 *
 * Name(s):Tyler Richardson  
 * NetID(s): tjr20
 *
 * NOTES:
 * 1. When using access_cache() you do not need to provide a "real" memory
 * addresses. You can use any convenient integer value as a memory address,
 * you should not be able to cause a segmentation fault by providing a memory
 * address out of your programs address space as the argument to access_cache.
 *
 * 2. Do NOT change the provided main function, especially the print statement.
 * If you do so, the autograder may fail to grade your code even if it produces
 * the correct result.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "support/mystery-cache.h"


/* Returns the size (in B) of each block in the cache. */
int get_block_size(void) {
	flush_cache();
 	int block_size = 0;
	int x = 0;
	unsigned long long address = 0x00000000;

	x = access_cache(address);//loads array into the cache
	
	for(int i = 0; i<5000;i++){//goes through each element of the array and if it is a cache hit, block_size increments and address increments to next element
	 x = access_cache(address+i); //checks next address for a hit or miss and returns if miss is detected
	 if(x!=1){
	 return i;
	 }
	}
	return block_size;
}


/* Returns the size (in B) of the cache. */
int get_cache_size(int block_size) {
  flush_cache();
  int cache_size = 1;
  int x=0;
  int loop_size = 0;
  unsigned long long address = 0x00000000;
  int l = 1;

  while(l==1){
   for(int i =0; i<= loop_size; i++){
    access_cache(address+(i*block_size));
   }
   x = access_cache(address);
   if(x!=1){
    return block_size*loop_size;
   }		
   loop_size++;
  }
  return cache_size;
}


/* Returns the associativity of the cache. */
int get_cache_assoc(int cache_size) {
        flush_cache();
	int loop_size=0;
	int block_size = 0;
	int address = 0x0;
    	int x;
    	int l=0;
	x = access_cache(address);//loads array into the cache

	while(l==0){
	 for(int j = 0; j<=loop_size; j++){
	  access_cache(address+(j*cache_size));
	 }
	  x = access_cache(address);
	  if(x!=1){
	   return loop_size;  
	  
	 }
	 loop_size++;
	}

  return block_size;
}


/* Run the functions above on a given cache and print the results. */
int main(int argc, char* argv[]) {
  int size;
  int assoc;
  int block_size;
  char do_block_size, do_size, do_assoc;
  do_block_size = do_size = do_assoc = 0;
  if (argc == 1) {
    do_block_size = do_size = do_assoc = 1;
  } else {
    for (int i = 1; i < argc; i++) {
      if (strcmp(argv[i], "block_size") == 0) {
        do_block_size = 1;
        continue;
      }
      if (strcmp(argv[i], "size") == 0) {
        do_size = 1;
        continue;
      }
      if (strcmp(argv[i], "assoc") == 0) {
        do_assoc = 1;
      }
    }
  }

  if (!do_block_size && !do_size && !do_assoc) {
    printf("No function requested!\n");
    printf("Usage: ./cache-test\n");
    printf("Usage: ./cache-test {block_size/size/assoc}\n");
    printf("\tyou may specify multiple functions\n");
    return EXIT_FAILURE;
  }

  cache_init(0, 0);

  block_size = size = assoc = -1;
  if (do_block_size) {
    block_size = get_block_size();
    printf("Cache block size: %d bytes\n", block_size);
  }
  if (do_size) {
    if (block_size == -1) block_size = get_block_size();
    size = get_cache_size(block_size);
    printf("Cache size: %d bytes\n", size);
  }
  if (do_assoc) {
    if (block_size == -1) block_size = get_block_size();
    if (size == -1) size = get_cache_size(block_size);
    assoc = get_cache_assoc(size);
    printf("Cache associativity: %d\n", assoc);
  }
}
