#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>



int main(){
	int x = 5;
	if(fork() != 0)
		printf("%d,",--x);

	printf("%d,",++x);
	exit(0);
}
